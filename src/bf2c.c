#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char const *template = "#include \"template.h\"\n{ ";
char const *outfilename = "a.c";
char const *bfchars = "+-><[],.";

char const *instr[] = {
    "AD",
    "SB",
    "PS",
    "PP",
    "JZ",
    "JP",
    "GT",
    "PT",
};

char const *
get_instr_name(char bf)
{
    return instr[strchr(bfchars, bf) - bfchars];
}

void
optimize(FILE *fin, FILE *fout, int chin)
{
    int ch;
    int nmatch = 1;

    while((ch = fgetc(fin)) != EOF){
        if(strchr(bfchars, ch) == NULL)
            continue;

        if(ch != chin){
            ungetc(ch, fin);
            break;
        }

        ++nmatch;
    }

    fprintf(fout, "%s(%u) ", get_instr_name(chin), nmatch);
}

void
compile(char *filename)
{
    FILE *fin = fopen(filename, "r");
    FILE *fout = fopen(outfilename, "w");
    int ch;

    fprintf(fout, "%s", template);

    while((ch = fgetc(fin)) != EOF){
        if(strchr(bfchars, ch) == NULL)
            continue;

        if(ch == '[' || ch == ']')
            fprintf(fout, "%s ", get_instr_name(ch));
        else
            optimize(fin, fout, ch);
    }

    fprintf(fout, "}");
    fclose(fin);
    fclose(fout);
}

void
usage(void)
{
    puts("bf2c - compile brainfuck to C");
    puts("usage: bf2c file");
    printf("generates %s\n", outfilename);
    exit(1);
}

int
main(int argc, char **argv)
{
    (void)(argc);

    if(argv[1] == NULL)
        usage();

    compile(argv[1]);

    return 0;
}
