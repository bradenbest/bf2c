int getchar(void);
int putchar(int);

char data[32768];
char *dataptr = data;

#define AD(n) *dataptr += n;
#define SB(n) *dataptr -= n;
#define PS(n) dataptr += n;
#define PP(n) dataptr -= n;
#define JZ    while(*dataptr){
#define JP    }
#define GT(n) for(int i = 0; i < n; ++i) *dataptr = getchar();
#define PT(n) for(int i = 0; i < n; ++i) putchar(*dataptr);

int main(void)
