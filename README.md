# bf2c

A quick n dirty brainfuck-to-C compiler

    $ make
    $ ./build.sh program.bf
    $ ./a.out

It compiles the code to a C file, taking inspiration from [bfvm2](https://gitlab.com/bradenbest/bfvm2)'s compiler in
that it crushes down repeated instructions to just a single instruction with an argument telling it how many times to
run.

## bfvm2 benchmark comparison

On my system, with test stdin input `n\nq\ny\nn\n` and program `lostkng.b`

    bf2c:
    compile time: 15 seconds
    runtime:      6ms
    filesize:     4MB

    bfvm:
    compile time: 200ms
    runtime:      23ms
    filesize:     285KiB

    bfvm (compiled with -O3):
    compile time: 190ms
    runtime:      16ms
    filesize:     285KiB

Remember that lost kingdom is a huge and complicated game, at a whopping 2MB of brainfuck code. The fact that bfvm2
generates the executable in a 1.3% of the time, at 14% the size of the source, and only takes 400% the time at runtime
(300% with -O3) is extremely impressive for bfvm2.

Basically the only reason you'd use bf2c is if you really, really need the brainfuck code to run as fast as possible,
since the generated x86 machine code runs about 3-4x faster than the VM.

Note: in bf2c's compile time metric, the runtime of gcc is also included (`time ./build.sh ../samples/lostkng.b`). bf2c
by itself finishes in about 60ms, which is faster than bfvm2 bfc. This is not unexpected, as bf2c is doing nothing
nontrivial. It wouldn't make sense to use bf2c by itself in the benchmark, though, as we're measuring the time it takes to
translate lost kingdom from brainfuck to an executable.
